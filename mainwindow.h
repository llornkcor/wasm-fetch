#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <emscripten/fetch.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    void doJsFetch();
    void doFetch();
    void doOldFetch();
    static void downloadProgress(emscripten_fetch_t *fetch);
    static void downloadFailed(emscripten_fetch_t *fetch);
    static void downloadSucceeded(emscripten_fetch_t *fetch);
    static void stateChange(emscripten_fetch_t *fetch);

};

#endif // MAINWINDOW_H
